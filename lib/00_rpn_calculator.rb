class RPNCalculator

  attr_accessor :total, :array

  def initialize
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def plus
    raise "calculator is empty" if @stack.size < 2
    result = @stack.pop + @stack.pop
    @stack << result
  end

  def value
    @stack.last
  end

  def minus
    raise "calculator is empty" if @stack.size < 2
    first = @stack.pop
    second = @stack.pop
    result = second - first
    @stack << result
  end

  def divide
      raise "calculator is empty" if @stack.size < 2
      first = @stack.pop.to_f
      second = @stack.pop.to_f
      result = (second / first)
      @stack << result
  end

  def times
    raise "calculator is empty" if @stack.size < 2
    first = @stack.pop.to_f
    second = @stack.pop.to_f
    result = (second * first)
    @stack << result
  end

  def tokens(string)
    tokens = string.split(" ")
    token_array = []
    tokens.each do |value|
      if value == "*"
        token_array << :*
      elsif value == "/"
        token_array << :/
      elsif value == "+"
        token_array << :+
      elsif value == "-"
        token_array << :-
      else
        token_array << value.to_i
      end
    end
    token_array
  end

  def evaluate(string)
    token_array = tokens(string)

    token_array.each_with_index do |value, idx|
      if value.is_a? Integer
        push(value)
        next
      elsif value == :*
        times
      elsif value == :/
        divide
      elsif value == :+
        plus
      elsif value == :-
        minus
      end

    end

    value
  end



end
